---
Title: Τα πρώτα μου βήματα στην υπολογιστική νέφους. Δωρεάν Εικονικός Ιδιωτικός Διακομιστής. Δημιουργία εικονικής μηχανής μέσω της δωρεάν κατηγορίας της Υποδομής Νέφους της Oracle.
Alternative Title: Ta prṓta mou bḗmata stēn ypologistikḗ néphous. Dōreán Eikonikós Idiōtikós Diakomistḗs. Dēmiourgía eikonikḗs mēchanḗs mésō tēs dōreán katēgorías tēs Ypodomḗs Néphous tēs Oracle.
Alternative Title: My first steps in cloud computing. Free Virtual Private Server. Create a virtual machine instance via Oracle cloud free tier.
Contributor: @nikaskonstantinos
Language: el
Date Issued: 18/11/2021
Publisher: Linux User Forum – Το forum των Ελλήνων χρηστών Linux – https://linux-user.gr/
Description: Cloud computing. Free VPS. Free virtual machine installation on the Oracle Cloud Infrastructure. Beginner's guide in Greek.
Category: Άρθρα και οδηγοί
Tags: OCI, oracle, cloud_computing, linux, server, ubuntu


---

## Περιεχόμενα:



[TOC]



## Εισαγωγή:

Στον οδηγό που ακολουθεί, θα δούμε πως μπορούμε να εγκαταστήσουμε δωρεάν μια εικονική μηχανή, μέσω της δωρεάν κατηγορίας της Υποδομής Νέφους της Oracle.

Συγκεκριμένα θα δημιουργήσουμε, έναν δωρεάν VPS(Virtual Private Server) και ειδικότερα, μια ubuntu εικονική μηχανή, αρχιτεκτονικής arm, 64bit με 4 Oracle vcpu και Oracle εικονική μνήμη 24 GB.

Η επιλογή είναι δική σας καθώς παρέχεται η δυνατότητα δημιουργίας δωρεάν VPS και με AMD αρχιτεκτονική.

## Δημιουργία Oracle λογαριασμού:

Για τη δημιουργία δωρεάν εικονικής μηχανής στην  Oracle απαιτείται δημιουργία λογαριασμού στην κατηγορία Oracle cloud free tier.

![oci02-2021-11-17_18-39.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci02-2021-11-17_18-39.png)

Εικόνα 1.



-  Για τη δημιουργία του λογαριασμού θα μας ζητηθούν, προς επιβεβαίωση της ταυτότητας μας, τα προσωπικά μας στοιχεία, στοιχεία πιστωτικής κάρτας και αριθμός κινητού τηλεφώνου.




![oci03-2021-11-17_18-41.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci03-2021-11-17_18-41.png)

Εικόνα 2.



## Επιλογή υπηρεσίας:

Μετά τη δημιουργία του λογαριασμού μας, πραγματοποιείται η ταυτοποίηση μας ως "Cloud Tenant" μέσω ηλεκτρονικής αλληλογραφίας και συνδεόμαστε στην **Oracle cloud console** στον διακομιστή της χώρας που επιλέξαμε κατά τη δημιουργία του λογαριασμού μας, 

- στη διεύθυνση 

<https://console.us-ashburn-1.oraclecloud.com/a/compute/images/ocid1.image.oc1.iad.aaaaaaaa2mnepqp7wn3ej2axm2nkoxwwcdwf7uc246tcltg4li67z6mktdiq> 

με το όνομα χρήστη(μισθωτή) και όχι ηλεκτρονικής αλληλογραφίας(όπως κατά λάθος φαίνεται στο παρακάτω στιγμιότυπο).



![oci08-2021-11-17_19-28.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci08-2021-11-17_19-28.png)

Εικόνα 3.



- Στη συνέχεια επιλέγουμε τη δωρεάν υπηρεσία που επιθυμούμε, δηλαδή τη δημιουργία εικονικής μηχανής.

![oci10-2021-11-17_19-33.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci10-2021-11-17_19-33.png)

Εικόνα 4.



## Δημιουργία Διακομιστή:



- Η προκαθορισμένη εικονική μηχανή είναι η Oracle Linux 7.9 amd, 1core, 1GB μνήμης.

![oci11-2021-11-17_19-42.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci11-2021-11-17_19-42.png)

Εικόνα 5.



- Πατώντας το πλήκτρο Change Image(παραπάνω), βλέπουμε επιπλέον επιλογές διακομιστών.

![oci12-2021-11-17_19-43.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci12-2021-11-17_19-43.png)

Εικόνα 6.



- Αφού επιλέξουμε τον διακομιστή μας (Select image), πατώντας το πλήκτρο Change Shape, επιλέγουμε την επιθυμητή αρχιτεκτονική της εικονικής μηχανής μας.

![oci14-2021-11-17_19-45.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci14-2021-11-17_19-45.png)

Εικόνα 7.



- Στην περίπτωσή μας επιλέχθηκε ο επεξεργαστής Ampere, βασισμένος σε Arm αρχιτεκτονική (αν χρησιμοποιώ τον σωστό όρο).

![oci16-2021-11-17_19-48.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci16-2021-11-17_19-48.png)

Εικόνα 8.



- Απαραίτητη διαδικασία είναι η λήψη και αποθήκευση των κλειδιών (Secure Shell), ssh(ιδιωτικό και δημόσιο), στον υπολογιστή μας, που απαιτούνται για τη σύνδεση στην Oracle εικονική μηχανή (VPS), που θα δημιουργήσουμε, από τον υπολογιστή μας.

  ![oci18-2021-11-17_19-51.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci18-2021-11-17_19-51.png)

Εικόνα 9.



- Τέλος, πατώντας το πλήκτρο Create (επάνω),  δημιουργούμε την εικονική μηχανή που επιλέξαμε, η οποία μετά από λίγο μπαίνει σε κατάσταση λειτουργίας.

![oci22-2021-11-17_19-59.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci22-2021-11-17_19-59.png)

Εικόνα 10.



## Σύνδεση στον διακομιστή:



Τώρα πλέον είμαστε έτοιμοι/ες, να συνδεθούμε στον Εικονικό Ιδιωτικό Διακομιστή μας, από τον υπολογιστή μας, σύμφωνα με τις αναλυτικές οδηγίες, τις οποίες βρίσκουμε στην ηλεκτρονική διεύθυνση:

<https://docs.oracle.com/en-us/iaas/Content/GSG/Tasks/testingconnection.htm>

- Σύμφωνα με το παρακάτω στιγμιότυπο, από την επίσημη σελίδα τεκμηρίωσης

![oci23-2021-11-17_20-02.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci23-2021-11-17_20-02.png)

Εικόνα 11.



- Δηλαδή μετά την αποθήκευση των κλειδιών ssh (ιδιωτικό, δημόσιο), στον υπολογιστή μας, συνδεόμαστε στον απομακρυσμένο διακομιστή που δημιουργήσαμε, με τις παρακάτω εντολές:

  παραχώρηση δικαιώματος ανάγνωσης του αρχείου,

```bash
chmod 400 <πλήρης_διαδρομή_αποθηκευμένου_ιδιωτικού_κλειδιου_ssh>
```

​		σύνδεση ssh στον διακομιστή με  όνομα χρήστη ubuntu για Ubuntu εικονική μηχανή, ή opc σε περίπτωση Centos εικονικής μηχανής

```bash
ssh -i <πλήρης_διαδρομή_αποθηκευμένου_ιδιωτικού_κλειδιου_ssh> <username_ubuntu_ή_opc>@<δημόσια_ip_του_διακομιστή μας>
```

![oci25-2021-11-17_20-13.png](https://gitlab.com/nikaskonstantinos/oci_post/-/raw/main/oci25-2021-11-17_20-13.png)

Εικόνα 12.



## Εκπαίδευση:

Ενδεικτικά, πιο αναλυτικές οδηγίες θα βρείτε στο πρόγραμμα, δωρεάν εκπαίδευσης στην υπολογιστική νέφους από την Oracle:

<https://cyberrubik.com/oracle-free-training-and-certification-for-oracle-cloud-infrastructure/#:~:text=Oracle%20announced%20it%20will%20offer%20Oracle%20Cloud%20Infrastructure,for%20all%20skill%20levels%20and%20various%20IT%20roles.>



## Συμπεράσματα:

Στον απλοϊκό αυτό οδηγό επιχειρήθηκε μια βηματική καταγραφή, δημιουργίας ενός εικονικού ιδιωτικού διακομιστή, στη δωρεάν κατηγορία της υποδομής νέφους της Oracle, η οποία εκτός των άλλων ειδικεύεται σε υπηρεσίες νέφους.

Μπορούμε με αυτόν τον τρόπο να πειραματιστούμε σε μικρά έργα, όπως για παράδειγμα στη δημιουργία και φιλοξενία προσωπικής ιστοσελίδας.

Ο οδηγός αυτός, όμως, θα μπορούσε, ιδανικά, να αποτελέσει τα πρώτα βήματα μας στην υπολογιστική νέφους και σε υλοποίηση παρεχόμενων υπηρεσιών, για την ανάπτυξη των δεξιοτήτων μας στον τομέα αυτό.

Έναν βιομηχανικό τομέα στον οποίο υπάρχει μεγάλη έλλειψη ειδικευμένου ανθρώπινου δυναμικού, τόσο στον ιδιωτικό όσο και στον δημόσιο τομέα, ειδικά στη χώρα μας.

Με συνέπεια να μην παρατηρείται η ανάλογη πρόοδος σύμφωνα με τις απαιτήσεις της εποχής μας, ιδιαίτερα στον τομέα των υπηρεσιών νέφους.

Ειδικά στο φόρουμ μας <https://linux-user.gr/>, έχουμε τη χαρά και την τιμή να συμμετέχουν έμπειρα στελέχη στον τομέα του cloud computing και την ηλεκτρονική διακυβέρνηση, φίλοι του linux, οι οποίοι θα μπορούσαν να μας αναλύσουν πολύ καλύτερα το πολύ σημαντικό αυτό θέμα, του οποίου επιχειρήθηκε μια συνοπτική παρουσίαση.

Τυχόν παρατηρήσεις είναι πάντα ευπρόσδεκτες και μπορούν να τεθούν εκτός από τα σχόλια και στην ηλεκτρονική διεύθυνση:

<https://gitlab.com/nikaskonstantinos/oci_post/-/issues>

Καλή επιτυχία! Μη διστάσετε να το δοκιμάσετε.

## Αναφορές:

- https://www.oracle.com/cloud/free/

- http://gsis.gr/dimosia-dioikisi/ked

- https://ec.europa.eu/isa2/eif_en

  
